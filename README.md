# Mini Bank Network

> This is an interactive, distributed, mini bank  demo. You can create customers,accounts, and move money from one account to another

This business network defines:

**Participants:**
`Customer`

**Assets:**
`Account`

**Transactions:**
`Account Transfer`



**To create network archive**
`composer archive create -a <business-network-archive>`

`composer archive create --sourceType dir --sourceName . -a ./project.bna`

`composer network deploy -a project.bna -A admin -S adminpw -c PeerAdmin@hlfv1 -f admincard`

`composer card import --file admincard --name admin@mini_bank_app`


**To Make data presistent**

add a volume to couchdb docker-compose.yml in this path /home/vagrant/fabric-dev-servers/fabric-scripts/hlfv1/composer/docker-compose.yml

`volumes: - /var/couchdb:/opt/couchdb/data`


**To create restful API with Swagger documentation**

`composer-rest-server -c admin@mini_bank_app -n never`
